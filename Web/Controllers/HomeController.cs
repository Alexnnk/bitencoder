﻿using EDFunctions;
using System;
using System.Text;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        private EncDec _encoder;

        public HomeController()
        {
            _encoder = new EncDec();
        }

        public ActionResult Index(string id)
        {
            ViewBag.Title = "Encoder";
            string message = "string from Home-Index";

            if (!string.IsNullOrEmpty(id)) message = id;

            ViewBag.Message = message;
            ViewBag.Before = Encoding.UTF7.GetBytes(message);
            ViewBag.Payload = _encoder.Encode(message);
            ViewBag.Decoding = _encoder.StrDecode(ViewBag.Payload);

            if (!string.IsNullOrEmpty(id)) return PartialView("Index");

            return View();
        }
    }
}
