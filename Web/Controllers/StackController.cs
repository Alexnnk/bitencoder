﻿using EDFunctions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Web.Models;

namespace Web.Controllers
{
    public class StackController : ApiController
    {
        private EncDec _encoder;

        public StackController()
        {
            _encoder = new EncDec();
        }

        private string GetStackData(Func<Stack<IEnumerable<byte>>, IEnumerable<byte>> predicate)
        {
            var b = predicate.Invoke(WebApiApplication.stackData).ToArray();
            return _encoder.StrDecode(b);
        }

        // GET data/stack
        public string Get()
        {
            string message;

            try
            {
                message = GetStackData(x => x.Peek());
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return message;
        }

        // GET data/stack/5
        public string Get(int id)
        {
            string message;

            try
            {
                if (id == 0)
                {
                    message = WebApiApplication.stackData.Count.ToString();
                }
                else message = GetStackData(x => x.Pop());
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            return message;
        }

        // POST data/stack
        public void Post([FromBody]SendMessage message)
        {
            var payload = _encoder.Encode(message.text);
            WebApiApplication.stackData.Push(payload);           
        }

        // PUT data/stack/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
