﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EDFunctions;

namespace Tests.Tests
{
    [TestClass]
    public class EncDecTest
    {
        [TestMethod]
        public void Encode()
        {
            // arrange
            EncDec compressor = new EncDec();
            // act
            byte[] result = compressor.Encode("dude");
            byte[] result2 = compressor.Encode("dudedude");
            // assets
            Assert.IsNotNull(result);
            CollectionAssert.AreEqual(new byte[4] { 228, 58, 185, 12 }, result);
            CollectionAssert.AreEqual(new byte[7] { 228, 58, 185, 76, 174, 147, 203 }, result2);
        }

        [TestMethod]
        public void StrDecode()
        {
            // arrange
            EncDec compressor = new EncDec();
            // act
            string result = compressor.StrDecode(new byte[4] { 228, 58, 185, 12 });
            string result2 = compressor.StrDecode(new byte[7] { 228, 58, 185, 76, 174, 147, 203 });
            // asset
            Assert.IsNotNull(result);
            Assert.AreEqual("dude", result);
            Assert.AreEqual("dudedude", result2);
        }
    }
}
