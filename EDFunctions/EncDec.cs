﻿using System;
using System.Collections;
using System.Linq;
using System.Text;

namespace EDFunctions
{
    public class EncDec
    {
        public byte[] Encode(string message)
        {
            if (message.Length < 2) throw new Exception("Сжатие не требуется");

            // Массив байтов входящего сообщения до сжатия
            byte[] byteMessage = Encoding.UTF7.GetBytes(message);
            int cnt = byteMessage.Count();

            // Массив упакованных байтов
            byte[] pack = new byte[cnt - cnt / 8];

            #region Упаковка 7 бит в байтовый массив

            // n - индекс, по которому будет добавление в новый массив
            // shift - значение сдвига
            byte nextByte = byteMessage[0];

            for (int i = 0, n = 0, shift = 0; i < cnt - 1; i++)
            {
                byte currentByte = (byte)(nextByte | (byteMessage[i + 1] << (8 - (shift + 1))));
                pack[n] = currentByte;

                if (++shift == 7)
                {
                    shift = 0;
                    if (cnt - i > 2)
                    {
                        nextByte = byteMessage[i++ + 2];
                        pack[n + 1] = nextByte;
                    }
                }
                else
                {
                    nextByte = (byte)((byteMessage[i + 1] >> shift));
                    pack[n + 1] = nextByte;
                }

                n++;
            }

            #endregion

            return pack;
        }

        public string StrDecode(byte[] payload)
        {
            StringBuilder sb = new StringBuilder();
            // Представление байтового массива в виде строки
            for (int i = payload.Length - 1; i > -1; i--)
            {
                string binStr = Convert.ToString(payload[i], 2).PadLeft(8, '0');
                sb.Append(binStr);
            }
            string allBinStr = sb.ToString();

            sb = new StringBuilder();

            // Декодированный массив
            int newLength = allBinStr.Length / 7;
            byte[] sevenBitChars = new byte[newLength];
            int n = newLength - 1;

            // Количество нулей, не использованных для кодировки 7 битных символов
            int otherZeros = allBinStr.Length % 7;
            for (int i = otherZeros, j = 0; i < allBinStr.Length; i++)
            {
                sb.Append(allBinStr[i]);
                if (++j % 7 == 0)
                {
                    sevenBitChars[n] = Convert.ToByte(sb.ToString(), 2);
                    sb.Clear();
                    n--;
                }
            }

            return Encoding.UTF7.GetString(sevenBitChars);
        }

        public byte[] StrEncode(string message)
        {
            byte[] byteMessage = Encoding.UTF7.GetBytes(message);
            // BitArray bitArray = new BitArray(byteMessage);

            // Размер байтового массива до сжатия
            int cnt = byteMessage.Count();

            char[] bits = new char[cnt * 7];
            ToBitsArray(ref bits, byteMessage);

            StringBuilder sb = new StringBuilder();

            #region Нули, заполняющие последний байт упакованного массива

            int bitsCnt = bits.Count();
            int zerosCnt = 8 - bitsCnt % 8;
            if (zerosCnt != 8)
            {
                string zeros = new string('0', zerosCnt);
                sb.Append(zeros);
            }

            #endregion

            // Размер будущего упакованного массива
            int packCount = cnt - cnt / 8;
            byte[] pack = new byte[packCount];

            #region Упаковка 7 бит в байтовый массив 

            int start = bitsCnt - 1;
            for (int i = packCount - 1; i > -1; i--)
            {
                while (start > -1)
                {
                    sb.Append(bits[start]);
                    start--;

                    if (sb.Length == 8) break;
                }
                pack[i] = Convert.ToByte(sb.ToString(), 2);
                sb.Clear();
            }

            #endregion

            return pack;
        }
        // hi
        public string Decode(byte[] payload)
        {
            return "hi";
        }

        /// <summary>
        /// Преобразует байтовый массив в последовательность битов
        /// </summary>
        /// <param name="bitsArray">Массив, который следует заполнить</param>
        /// <param name="byteMessage">Массив, битами которого следует заполнить bitsArray</param>
        /// <returns></returns>
        private void ToBitsArray(ref char[] bitsArray, byte[] byteMessage)
        {
            int j = bitsArray.Count() - 1;

            for (int i = byteMessage.Count() - 1; i > -1; i--)
            {
                var currentByteChars = Convert
                    .ToString(byteMessage[i], 2)
                    .PadLeft(7, '0');

                foreach (var b in currentByteChars)
                {
                    bitsArray[j] = b;
                    j--;
                }
            }
        }
    }
}
